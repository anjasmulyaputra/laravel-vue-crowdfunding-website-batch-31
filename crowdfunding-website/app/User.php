<?php

namespace App;

use Illuminate\Support\Str;
use App\Models\Role;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = ["username", "email", "name", "role_id"];
    protected $primaryKey = 'id';
    protected $keyTpye = 'string';
    public $incrementing = false;

    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (!$model->{$model->getKeyName()}) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }
}
